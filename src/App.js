import React from 'react'
import Navigation from './config/routes'

class App extends React.Component {
  render() {
    return (
      <Navigation />
    )
  }
}

export default App