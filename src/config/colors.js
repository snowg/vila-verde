const colors = {
  accent: '#cd1500',
  primary: '#9e9e9e',
  primaryDark: '#212121',
  background: 'white',
  shadow: '#666666',
  white: 'white',
  gray: '#f5f5f5',
  header: '#f5f5f5',
  headerItems: '#212121'
}

export default colors