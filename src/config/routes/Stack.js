/* Arquivo de configuração do navigator estilo stack */
import React from 'react'
import { createStackNavigator,createAppContainer } from 'react-navigation'
import colors from '../colors'

// Import das telas a serem navegadas
import Home from '../../containers/Home'
import Question from '../../containers/Question'

const Stack = createAppContainer(
  createStackNavigator(
    {
      Home: { screen: Home, navigationOptions: { header: null } },
      Question: { screen: Question, navigationOptions: { header: null } }
    },
    { 
      initialRouteName: 'Home',
      headerBackTitleVisible: false,
  
      navigationOptions: {
        swipeEnabled: false,
        tabBarVisible: true,
  
        headerLeft: null,
        headerRight: null,
        headerStyle: {
          backgroundColor: colors.header,
        },
        headerTintColor: colors.headerItems,
        headerTitleStyle: { 
          fontSize: '15', 
          fontWeight: 'bold'
        },
      },
    }
  )
)


export default Stack